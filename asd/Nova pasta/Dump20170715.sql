-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 104.198.16.25    Database: hawinsoft_dialer
-- ------------------------------------------------------
-- Server version	5.7.14-google-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='330f22c0-f46f-11e6-98f1-42010a80053e:1-13742772';

--
-- Table structure for table `hawin_adicaovendadiavencimento`
--

DROP TABLE IF EXISTS `hawin_adicaovendadiavencimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicaovendadiavencimento` (
  `id_adicaovendadiavencimento` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dia_vencimento` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicaovendadiavencimento`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicaovendadiavencimento`
--

LOCK TABLES `hawin_adicaovendadiavencimento` WRITE;
/*!40000 ALTER TABLE `hawin_adicaovendadiavencimento` DISABLE KEYS */;
INSERT INTO `hawin_adicaovendadiavencimento` VALUES (1,'2017-06-14 01:40:40','2','2',1),(2,'2017-06-14 01:40:40','5','5',1),(3,'2017-06-14 01:40:40','8','8',1),(4,'2017-06-14 01:40:40','15','15',1),(5,'2017-06-14 01:40:40','21','21',1),(6,'2017-06-14 01:40:40','25','25',1);
/*!40000 ALTER TABLE `hawin_adicaovendadiavencimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_adicaovendapacote`
--

DROP TABLE IF EXISTS `hawin_adicaovendapacote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicaovendapacote` (
  `id_adicaovendapacote` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_pacote` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicaovendapacote`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicaovendapacote`
--

LOCK TABLES `hawin_adicaovendapacote` WRITE;
/*!40000 ALTER TABLE `hawin_adicaovendapacote` DISABLE KEYS */;
INSERT INTO `hawin_adicaovendapacote` VALUES (1,'2017-06-14 01:19:49','TELECINE R$ 39.90','TELECINE R$ 39.90',1),(2,'2017-06-14 01:19:49','HBO R$ 29.90','HBO R$ 29.90',1),(3,'2017-06-14 01:19:49','PREMIERE R$ 80.00','PREMIERE R$ 80.00',1),(4,'2017-06-14 01:19:49','PACOTE ESPORTES R$ 19.90','PACOTE ESPORTES R$ 19.90',1),(5,'2017-06-14 01:19:49','SEX HOT R$ 43.90','SEX HOT R$ 43.90',1),(6,'2017-06-14 01:19:49','COMBATE R$ 62.90','COMBATE R$ 62.90',1);
/*!40000 ALTER TABLE `hawin_adicaovendapacote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_adicaovendaplanodados`
--

DROP TABLE IF EXISTS `hawin_adicaovendaplanodados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicaovendaplanodados` (
  `id_adicaovendaplanodados` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_plano` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicaovendaplanodados`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicaovendaplanodados`
--

LOCK TABLES `hawin_adicaovendaplanodados` WRITE;
/*!40000 ALTER TABLE `hawin_adicaovendaplanodados` DISABLE KEYS */;
INSERT INTO `hawin_adicaovendaplanodados` VALUES (1,'2017-06-14 01:11:28','15 MB','15 MB',1),(2,'2017-06-14 01:11:28','25 MB','25 MB',1),(3,'2017-06-14 01:11:28','50 MB','50 MB',1),(4,'2017-06-14 01:11:28','100 MB','100 MB',1),(5,'2017-06-14 01:11:28','200 MB','200 MB',1),(6,'2017-06-14 01:11:28','300 MB','300 MB',1);
/*!40000 ALTER TABLE `hawin_adicaovendaplanodados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_adicaovendaplanotv`
--

DROP TABLE IF EXISTS `hawin_adicaovendaplanotv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicaovendaplanotv` (
  `id_adicaovendaplanotv` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_plano` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicaovendaplanotv`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicaovendaplanotv`
--

LOCK TABLES `hawin_adicaovendaplanotv` WRITE;
/*!40000 ALTER TABLE `hawin_adicaovendaplanotv` DISABLE KEYS */;
INSERT INTO `hawin_adicaovendaplanotv` VALUES (1,'2017-06-14 01:09:36','SUPER HD','SUPER HD',1),(2,'2017-06-14 01:09:36','SUPER HD + FUTEBOL','SUPER HD + FUTEBOL',1),(3,'2017-06-14 01:09:36','ULTRA HD','ULTRA HD',1),(4,'2017-06-14 01:09:36','ULTRA HD + FUTEBOL','ULTRA HD + FUTEBOL',1),(5,'2017-06-14 01:09:36','ULTIMATE HD','ULTIMATE HD',1),(6,'2017-06-14 01:09:36','ULTIMATE HD + FUTEBOL','ULTIMATE HD + FUTEBOL',1),(7,'2017-06-14 01:09:36','FULL HD','FULL HD',1),(8,'2017-06-14 01:09:36','FULL HD + FUTEBOL','FULL HD + FUTEBOL',1);
/*!40000 ALTER TABLE `hawin_adicaovendaplanotv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_adicaovendaplanovoz`
--

DROP TABLE IF EXISTS `hawin_adicaovendaplanovoz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicaovendaplanovoz` (
  `id_adicaovendaplanovoz` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_plano` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicaovendaplanovoz`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicaovendaplanovoz`
--

LOCK TABLES `hawin_adicaovendaplanovoz` WRITE;
/*!40000 ALTER TABLE `hawin_adicaovendaplanovoz` DISABLE KEYS */;
INSERT INTO `hawin_adicaovendaplanovoz` VALUES (1,'2017-06-14 00:51:45','ILIMITADO LOCAL','ILIMITADO LOCAL',1),(2,'2017-06-14 00:52:02','ILIMITADO MÓVEL LOCAL','ILIMITADO MÓVEL LOCAL',1),(3,'2017-06-14 00:52:11','ILIMITADO MÓVEL BRASIL','ILIMITADO MÓVEL BRASIL',1);
/*!40000 ALTER TABLE `hawin_adicaovendaplanovoz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_adicionarvenda`
--

DROP TABLE IF EXISTS `hawin_adicionarvenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_adicionarvenda` (
  `id_adicionarvenda` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_cliente` varchar(225) NOT NULL,
  `nome_mae` varchar(225) NOT NULL,
  `nascimento` varchar(225) NOT NULL,
  `cpf` varchar(225) NOT NULL,
  `nome_solicitante` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `telefone_1` varchar(15) NOT NULL,
  `telefone_2` varchar(15) DEFAULT NULL,
  `telefone_3` varchar(15) DEFAULT NULL,
  `vencimento` int(2) NOT NULL,
  `endereco_ins` varchar(225) NOT NULL,
  `numero_ins` varchar(225) NOT NULL,
  `bairro_ins` varchar(225) NOT NULL,
  `cidade_ins` varchar(225) NOT NULL,
  `complemento_ins` varchar(225) NOT NULL,
  `estado_ins` varchar(225) NOT NULL,
  `cep_ins` varchar(10) NOT NULL,
  `endereco_cob` varchar(225) NOT NULL,
  `numero_cob` varchar(225) NOT NULL,
  `bairro_cob` varchar(225) NOT NULL,
  `cidade_cob` varchar(225) NOT NULL,
  `complemento_cob` varchar(225) NOT NULL,
  `estado_cob` varchar(225) NOT NULL,
  `cep_cob` varchar(10) NOT NULL,
  `id_adicaovendaplanovoz` int(10) NOT NULL,
  `id_adicaovendaplanodados` int(10) NOT NULL,
  `id_adicaovendaplanotv` int(10) NOT NULL,
  `pacotes` text NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `id_permissao` int(10) NOT NULL,
  `id_vistovenda` int(10) NOT NULL,
  `criacao_visto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_usuario_backoffice` int(10) DEFAULT NULL,
  `observacoes` text NOT NULL,
  `vizualizado` tinyint(1) NOT NULL DEFAULT '1',
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_adicionarvenda`),
  KEY `id_adicaovendaplanovoz` (`id_adicaovendaplanovoz`),
  KEY `id_adicaovendaplanodados` (`id_adicaovendaplanodados`),
  KEY `id_adicaovendaplanotv` (`id_adicaovendaplanotv`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_permissao` (`id_permissao`),
  KEY `id_vistovenda` (`id_vistovenda`),
  CONSTRAINT `fk_adicaovendaplanodados` FOREIGN KEY (`id_adicaovendaplanodados`) REFERENCES `hawin_adicaovendaplanodados` (`id_adicaovendaplanodados`),
  CONSTRAINT `fk_adicaovendaplanotv` FOREIGN KEY (`id_adicaovendaplanotv`) REFERENCES `hawin_adicaovendaplanotv` (`id_adicaovendaplanotv`),
  CONSTRAINT `fk_adicaovendaplanovoz` FOREIGN KEY (`id_adicaovendaplanovoz`) REFERENCES `hawin_adicaovendaplanovoz` (`id_adicaovendaplanovoz`),
  CONSTRAINT `fk_permissao_adicionarvenda` FOREIGN KEY (`id_permissao`) REFERENCES `hawin_permissao` (`id_permissao`),
  CONSTRAINT `fk_usuario_adicionarvenda` FOREIGN KEY (`id_usuario`) REFERENCES `hawin_usuario` (`id_usuario`),
  CONSTRAINT `fk_vistovenda` FOREIGN KEY (`id_vistovenda`) REFERENCES `hawin_vistovenda` (`id_vistovenda`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_adicionarvenda`
--

LOCK TABLES `hawin_adicionarvenda` WRITE;
/*!40000 ALTER TABLE `hawin_adicionarvenda` DISABLE KEYS */;
INSERT INTO `hawin_adicionarvenda` VALUES (1,'2017-06-19 02:11:32','FELIPE EDUARDO','DE OLIVEIRA','2017-06-16','08307213479','FELIPE EDUARDO SOUZA DE OLIVEIRA','f.l.p.eduardo@hotmail.com','81997947555','81997947555','81997947555',1,'Rua Mataripe','123','jardim','Olinda','casa','PE','53300300','Rua Mataripe','123','jardim','Olinda','casa','PE','53300300',1,1,1,'TELECINE R$ 39.90;HBO R$ 29.90',1,1,1,'0000-00-00 00:00:00',0,'teste',1,1),(2,'2017-06-19 02:14:30','FELIPE EDUARDO','DE OLIVEIRA','2017-06-21','08307213479','FELIPE EDUARDO SOUZA DE OLIVEIRA','f.l.p.eduardo@hotmail.com','81997947555','81997947555','81997947555',1,'Rua Mataripe','123','jardim','Olinda','casa','PE','53300300','Rua Mataripe','123','jardim','Olinda','casa','PE','53300300',1,1,1,'TELECINE R$ 39.90;HBO R$ 29.90',1,1,2,'2017-07-02 22:10:38',1,'teste edição',1,1),(3,'2017-07-02 16:11:11','FELIPE EDUARDO','DE OLIVEIRA','1989-12-30','08307213479','FELIPE EDUARDO SOUZA DE OLIVEIRA','f.l.p.eduardo@hotmail.com','81997947555','81997947555','81997947555',1,'Rua Mataripe','123','TESTE','Olinda','casa','PE','53300300','Rua Mataripe','123','TESTE','Olinda','casa','PE','53300300',1,1,1,'TELECINE R$ 39.90',1,1,2,'2017-07-13 02:07:46',1,'teste ok',1,1),(4,'2017-07-02 16:28:30','FELIPE EDUARDO','DE OLIVEIRA','1989-04-30','08307213479','FELIPE EDUARDO SOUZA DE OLIVEIRA','f.l.p.eduardo@hotmail.com','81997947555','81997947555','81997947555',1,'Rua Mataripe','140','TESTE','Olinda','casa','PE','53300300','Rua Mataripe','140','TESTE','Olinda','casa','PE','53300300',1,1,1,'PREMIERE R$ 80.00;PACOTE ESPORTES R$ 19.90',1,1,3,'2017-07-13 02:42:16',1,'teste ok 2',1,1),(5,'2017-07-02 21:51:49','FELIPE EDUARDO','DE OLIVEIRA','2017-07-02','08307213479','FELIPE EDUARDO SOUZA DE OLIVEIRA','f.l.p.eduardo@hotmail.com','81997947555','81997947555','81997947555',1,'Rua Mataripe','140','TESTE','Olinda','casa','PE','53300300','Rua Mataripe','140','TESTE','Olinda','casa','PE','53300300',1,1,1,'TELECINE R$ 39.90;HBO R$ 29.90;PREMIERE R$ 80.00;PACOTE ESPORTES R$ 19.90;SEX HOT R$ 43.90;COMBATE R$ 62.90',1,1,2,'2017-07-12 02:57:42',1,'teste',1,1),(6,'2017-07-10 01:37:36','ATHOS BONNER1','JUREMA1','1992-02-06','17215443728','ATHOS BONNER1','athos.sti@hotmal.com1','999999999','999999999','999999999221',6,'Rua notifis dos palmares 1','111111113333111111111111111111111111111111111111111111111111111111199999999999999999999999999993333333333333333333333399999999999999999999','1Aguas de Vikins ivadindo a inglaterra','valhalla 1','Com 12 virgens 1','PE','31190010','Torre Stark 1','098288888828828288228','Nova York1','Mark 441','Não deixar os Thanos fominar a terra 1','PE','53190010',2,3,2,'TELECINE R$ 39.90;HBO R$ 29.90;PREMIERE R$ 80.00;PACOTE ESPORTES R$ 19.90;SEX HOT R$ 43.90;COMBATE R$ 62.90',4,1,2,'2017-07-13 02:08:08',2,'Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, ',1,1);
/*!40000 ALTER TABLE `hawin_adicionarvenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_agente`
--

DROP TABLE IF EXISTS `hawin_agente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_agente` (
  `id_agente` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_agente` varchar(20) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_agente`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `fk_agente_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `hawin_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_agente`
--

LOCK TABLES `hawin_agente` WRITE;
/*!40000 ALTER TABLE `hawin_agente` DISABLE KEYS */;
INSERT INTO `hawin_agente` VALUES (1,'2017-05-23 23:50:01','10000',1,1);
/*!40000 ALTER TABLE `hawin_agente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_atendimento`
--

DROP TABLE IF EXISTS `hawin_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_atendimento` (
  `id_atendimento` bigint(19) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_agente` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `id_frontend_categoria` int(10) NOT NULL,
  `id_frontend_atendimento` int(10) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `ramal` varchar(20) NOT NULL,
  `observacoes` text,
  `tempo` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_atendimento`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_agente` (`id_agente`),
  KEY `id_frontend_categoria` (`id_frontend_categoria`),
  KEY `id_frontend_atendimento` (`id_frontend_atendimento`),
  CONSTRAINT `fk_atendimento_agente` FOREIGN KEY (`id_agente`) REFERENCES `hawin_agente` (`id_agente`),
  CONSTRAINT `fk_atendimento_atendimento` FOREIGN KEY (`id_frontend_atendimento`) REFERENCES `hawin_frontend_atendimento` (`id_frontend_atendimento`),
  CONSTRAINT `fk_atendimento_categoria` FOREIGN KEY (`id_frontend_categoria`) REFERENCES `hawin_frontend_categoria` (`id_frontend_categoria`),
  CONSTRAINT `fk_atendimento_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `hawin_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_atendimento`
--

LOCK TABLES `hawin_atendimento` WRITE;
/*!40000 ALTER TABLE `hawin_atendimento` DISABLE KEYS */;
INSERT INTO `hawin_atendimento` VALUES (1,'2017-05-24 23:31:26',1,1,2,3,1,'20000','','00:00:06',1),(2,'2017-05-24 23:31:54',1,1,2,3,1,'20000','TESTE OK','00:00:34',1),(3,'2017-05-24 23:52:21',1,1,1,1,1,'20000','','00:00:22',1),(4,'2017-05-24 23:52:59',1,1,3,6,1,'20000','TESTE','00:00:59',1),(5,'2017-05-24 23:53:16',1,1,2,4,1,'20000','TESTE OK','00:01:17',1),(6,'2017-05-24 23:57:42',1,1,2,3,1,'20000','','00:00:06',1),(7,'2017-05-24 23:58:01',1,1,3,6,1,'20000','TESTE TESTE OK ','00:00:25',1);
/*!40000 ALTER TABLE `hawin_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_carga`
--

DROP TABLE IF EXISTS `hawin_carga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_carga` (
  `id_carga` bigint(19) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_servico` bigint(19) NOT NULL,
  `nome_carga` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_carga`),
  KEY `id_servico` (`id_servico`),
  CONSTRAINT `fk_carga_servico` FOREIGN KEY (`id_servico`) REFERENCES `hawin_servico` (`id_servico`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_carga`
--

LOCK TABLES `hawin_carga` WRITE;
/*!40000 ALTER TABLE `hawin_carga` DISABLE KEYS */;
INSERT INTO `hawin_carga` VALUES (1,'2017-05-24 22:37:15',1,'CARGA TESTE',1);
/*!40000 ALTER TABLE `hawin_carga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_cliente`
--

DROP TABLE IF EXISTS `hawin_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_cliente` (
  `id_cliente` bigint(19) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_carga` bigint(19) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `nome_cliente` varchar(225) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `personalizado1` text,
  `personalizado2` text,
  `personalizado3` text,
  `personalizado4` text,
  `personalizado5` text,
  `personalizado6` text,
  `personalizado7` text,
  `personalizado8` text,
  `personalizado9` text,
  `personalizado10` text,
  `personalizado11` text,
  `personalizado12` text,
  `personalizado13` text,
  `personalizado14` text,
  `personalizado15` text,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cliente`),
  KEY `id_carga` (`id_carga`),
  CONSTRAINT `fk_cliente_carga` FOREIGN KEY (`id_carga`) REFERENCES `hawin_carga` (`id_carga`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_cliente`
--

LOCK TABLES `hawin_cliente` WRITE;
/*!40000 ALTER TABLE `hawin_cliente` DISABLE KEYS */;
INSERT INTO `hawin_cliente` VALUES (1,'2017-05-24 22:43:00',1,'81997947555','CLIENTE TESTE','08307213479',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `hawin_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_frontend_atendimento`
--

DROP TABLE IF EXISTS `hawin_frontend_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_frontend_atendimento` (
  `id_frontend_atendimento` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_atendimento` varchar(20) NOT NULL,
  `id_frontend_categoria` int(10) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_frontend_atendimento`),
  KEY `id_frontend_categoria` (`id_frontend_categoria`),
  CONSTRAINT `fk_categoria_atendimento` FOREIGN KEY (`id_frontend_categoria`) REFERENCES `hawin_frontend_categoria` (`id_frontend_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_frontend_atendimento`
--

LOCK TABLES `hawin_frontend_atendimento` WRITE;
/*!40000 ALTER TABLE `hawin_frontend_atendimento` DISABLE KEYS */;
INSERT INTO `hawin_frontend_atendimento` VALUES (1,'2017-05-23 20:37:26','ATENDIMENTO - 1',1,1),(2,'2017-05-23 20:37:30','ATENDIMENTO - 2',1,1),(3,'2017-05-23 20:37:36','ATENDIMENTO - 3',2,1),(4,'2017-05-23 20:37:39','ATENDIMENTO - 4',2,1),(5,'2017-05-23 20:37:58','ATENDIMENTO - 5',3,1),(6,'2017-05-23 20:38:02','ATENDIMENTO - 6',3,1);
/*!40000 ALTER TABLE `hawin_frontend_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_frontend_categoria`
--

DROP TABLE IF EXISTS `hawin_frontend_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_frontend_categoria` (
  `id_frontend_categoria` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_categoria` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_frontend_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_frontend_categoria`
--

LOCK TABLES `hawin_frontend_categoria` WRITE;
/*!40000 ALTER TABLE `hawin_frontend_categoria` DISABLE KEYS */;
INSERT INTO `hawin_frontend_categoria` VALUES (1,'2017-05-23 20:30:14','CATEGORIA - 1',1),(2,'2017-05-23 20:30:18','CATEGORIA - 2',1),(3,'2017-05-23 20:30:21','CATEGORIA - 3',1);
/*!40000 ALTER TABLE `hawin_frontend_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_frontend_log`
--

DROP TABLE IF EXISTS `hawin_frontend_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_frontend_log` (
  `id_frontend_log` bigint(19) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_agente` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `evento` varchar(225) NOT NULL,
  `descricao` text NOT NULL,
  `tempo` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_frontend_log`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_agente` (`id_agente`),
  CONSTRAINT `fk_log_agente` FOREIGN KEY (`id_agente`) REFERENCES `hawin_agente` (`id_agente`),
  CONSTRAINT `fk_log_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `hawin_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_frontend_log`
--

LOCK TABLES `hawin_frontend_log` WRITE;
/*!40000 ALTER TABLE `hawin_frontend_log` DISABLE KEYS */;
INSERT INTO `hawin_frontend_log` VALUES (1,'2017-05-24 20:03:53',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(2,'2017-05-24 20:03:55',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:03',1),(3,'2017-05-24 20:04:00',1,1,'RETORNO : 81997947555','AGENTE RETORNOU CONTATO','00:00:00',1),(4,'2017-05-24 20:04:06',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:09',1),(5,'2017-05-24 20:42:14',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(6,'2017-05-24 20:42:15',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:01',1),(7,'2017-05-24 20:42:43',1,1,'RETORNO : 81997947555','AGENTE RETORNOU CONTATO','00:00:00',1),(8,'2017-05-24 20:42:47',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:31',1),(9,'2017-05-24 23:02:14',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(10,'2017-05-24 23:02:23',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:10',1),(11,'2017-05-24 23:02:39',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:14',1),(12,'2017-05-24 23:03:19',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:54',1),(13,'2017-05-24 23:11:10',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(14,'2017-05-24 23:11:11',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:02',1),(15,'2017-05-24 23:11:55',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:42',1),(16,'2017-05-24 23:16:01',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(17,'2017-05-24 23:16:03',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:02',1),(18,'2017-05-24 23:16:27',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:23',1),(19,'2017-05-24 23:21:17',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(20,'2017-05-24 23:21:18',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:01',1),(21,'2017-05-24 23:21:35',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:16',1),(22,'2017-05-24 23:21:44',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:26',1),(23,'2017-05-24 23:23:26',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(24,'2017-05-24 23:23:26',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:01',1),(25,'2017-05-24 23:23:35',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:07',1),(26,'2017-05-24 23:23:53',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:25',1),(27,'2017-05-24 23:27:27',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(28,'2017-05-24 23:28:48',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:01:22',1),(29,'2017-05-24 23:29:17',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:27',1),(30,'2017-05-24 23:29:27',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:37',1),(31,'2017-05-24 23:31:13',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(32,'2017-05-24 23:31:18',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:07',1),(33,'2017-05-24 23:32:11',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:52',1),(34,'2017-05-24 23:51:52',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(35,'2017-05-24 23:51:56',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:05',1),(36,'2017-05-24 23:52:20',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:22',1),(37,'2017-05-24 23:52:57',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:59',1),(38,'2017-05-24 23:53:15',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:01:17',1),(39,'2017-05-24 23:53:49',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:01:51',1),(40,'2017-05-24 23:57:31',1,1,'LOGIN AGENTE','LOGIN AGENTE FRONTEND','00:00:00',1),(41,'2017-05-24 23:57:34',1,1,'AGENTE ACW','AGENTE CLICOU EM DISPONIVEL APOS UM DETERMINADO TEMPO EM ACW','00:00:05',1),(42,'2017-05-24 23:57:42',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:06',1),(43,'2017-05-24 23:58:01',1,1,'ATENDIMENTO','AGENTE GRAVOU ATENDIMENTO','00:00:25',1),(44,'2017-05-24 23:58:07',1,1,'LOGOFF AGENTE','LOGOFF AGENTE FRONTEND','00:00:31',1);
/*!40000 ALTER TABLE `hawin_frontend_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_frontend_parada`
--

DROP TABLE IF EXISTS `hawin_frontend_parada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_frontend_parada` (
  `id_frontend_parada` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_parada` varchar(20) NOT NULL,
  `descricao` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_frontend_parada`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_frontend_parada`
--

LOCK TABLES `hawin_frontend_parada` WRITE;
/*!40000 ALTER TABLE `hawin_frontend_parada` DISABLE KEYS */;
INSERT INTO `hawin_frontend_parada` VALUES (1,'2017-05-23 20:25:28','PARADA - 1','PARADA - 1',1),(2,'2017-05-23 20:25:36','PARADA - 2','PARADA - 2',1),(3,'2017-05-23 20:25:45','PARADA - 3','PARADA - 3',1);
/*!40000 ALTER TABLE `hawin_frontend_parada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_historicovistovenda`
--

DROP TABLE IF EXISTS `hawin_historicovistovenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_historicovistovenda` (
  `id_historicovistovenda` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_adicionarvenda` int(10) NOT NULL,
  `id_vistovenda` int(10) NOT NULL,
  `id_usuario_backoffice` int(10) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_historicovistovenda`),
  KEY `id_adicionarvenda` (`id_adicionarvenda`),
  KEY `id_vistovenda` (`id_vistovenda`),
  KEY `id_usuario_backoffice` (`id_usuario_backoffice`),
  CONSTRAINT `fk_his_adicionarvenda` FOREIGN KEY (`id_adicionarvenda`) REFERENCES `hawin_adicionarvenda` (`id_adicionarvenda`),
  CONSTRAINT `fk_his_usuario_backoffice` FOREIGN KEY (`id_usuario_backoffice`) REFERENCES `hawin_usuario` (`id_usuario`),
  CONSTRAINT `fk_his_vistovenda` FOREIGN KEY (`id_vistovenda`) REFERENCES `hawin_vistovenda` (`id_vistovenda`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_historicovistovenda`
--

LOCK TABLES `hawin_historicovistovenda` WRITE;
/*!40000 ALTER TABLE `hawin_historicovistovenda` DISABLE KEYS */;
INSERT INTO `hawin_historicovistovenda` VALUES (1,'2017-07-02 22:13:42',3,2,1,1),(2,'2017-07-02 22:17:05',3,2,1,1),(3,'2017-07-05 14:12:25',3,2,3,1),(4,'2017-07-10 01:00:15',3,3,4,1),(5,'2017-07-10 01:01:03',3,1,4,1),(6,'2017-07-10 01:01:08',3,1,4,1),(7,'2017-07-10 01:01:11',3,2,4,1),(8,'2017-07-10 01:01:13',3,2,4,1),(9,'2017-07-10 01:01:15',3,2,4,1),(10,'2017-07-10 01:01:21',3,1,4,1),(11,'2017-07-10 01:01:25',3,2,4,1),(12,'2017-07-10 01:01:40',3,2,4,1),(13,'2017-07-10 01:02:14',3,2,4,1),(14,'2017-07-10 01:02:32',3,2,4,1),(15,'2017-07-10 01:02:57',3,2,4,1),(16,'2017-07-10 01:04:01',3,2,4,1),(17,'2017-07-10 01:39:06',6,2,4,1),(18,'2017-07-10 01:41:34',6,3,4,1),(19,'2017-07-12 02:57:42',5,2,1,1),(20,'2017-07-13 02:06:22',3,2,2,1),(21,'2017-07-13 02:07:46',3,2,1,1),(22,'2017-07-13 02:08:08',6,2,2,1),(23,'2017-07-13 02:42:16',4,3,1,1);
/*!40000 ALTER TABLE `hawin_historicovistovenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_permissao`
--

DROP TABLE IF EXISTS `hawin_permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_permissao` (
  `id_permissao` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_permissao` varchar(20) NOT NULL,
  `descricao` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_permissao`
--

LOCK TABLES `hawin_permissao` WRITE;
/*!40000 ALTER TABLE `hawin_permissao` DISABLE KEYS */;
INSERT INTO `hawin_permissao` VALUES (1,'2017-05-23 23:41:55','DESENVOLVIMENTO','DESENVOLVIMENTO',1),(2,'2017-05-29 17:55:46','ADMINISTRACAO','ADMINISTRACAO',1),(3,'2017-05-29 17:56:12','SUPERVISAO','SUPERVISAO',1),(4,'2017-05-29 17:56:34','AGENTE INTERNO','AGENTE INTERNO',1),(5,'2017-06-08 17:58:34','AGENTE EXTERNO','AGENTE EXTERNO',1);
/*!40000 ALTER TABLE `hawin_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_ponto_digital`
--

DROP TABLE IF EXISTS `hawin_ponto_digital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_ponto_digital` (
  `id_ponto_digital` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario` varchar(225) NOT NULL,
  `nome_usuario` varchar(20) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_ponto_digital`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `fk_ponto_digital_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `hawin_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_ponto_digital`
--

LOCK TABLES `hawin_ponto_digital` WRITE;
/*!40000 ALTER TABLE `hawin_ponto_digital` DISABLE KEYS */;
INSERT INTO `hawin_ponto_digital` VALUES (1,'2017-06-01 18:09:24','08307213479','FELIPE EDUARDO SOUZA',1,1),(2,'2017-06-01 18:09:39','08307213479','FELIPE EDUARDO SOUZA',1,1),(3,'2017-06-08 19:47:48','08307213479','FELIPE EDUARDO SOUZA',1,1),(4,'2017-06-26 01:23:51','08307213479','FELIPE EDUARDO SOUZA',1,1),(5,'2017-07-02 15:07:29','08307213479','FELIPE EDUARDO SOUZA',1,1),(6,'2017-07-06 00:38:11','08307213479','FELIPE EDUARDO SOUZA',1,1),(7,'2017-07-06 01:18:26','08307213479','FELIPE EDUARDO SOUZA',1,1),(8,'2017-07-10 02:02:25','10322491452','ATHOS BONNER',4,1),(9,'2017-07-10 23:37:27','08448077458','DERICK PAULINO IRINE',2,1),(10,'2017-07-12 01:03:08','08307213479','FELIPE EDUARDO SOUZA',1,1),(11,'2017-07-13 02:11:32','08448077458','DERICK PAULINO IRINE',2,1),(12,'2017-07-13 02:12:44','08419788406','WAGNER FRANÇA',5,1),(13,'2017-07-13 02:14:06','08419788406','WAGNER FRANÇA',5,1);
/*!40000 ALTER TABLE `hawin_ponto_digital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_servico`
--

DROP TABLE IF EXISTS `hawin_servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_servico` (
  `id_servico` bigint(19) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_servico` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_servico`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_servico`
--

LOCK TABLES `hawin_servico` WRITE;
/*!40000 ALTER TABLE `hawin_servico` DISABLE KEYS */;
INSERT INTO `hawin_servico` VALUES (1,'2017-05-24 22:36:13','SERVICO TESTE',1);
/*!40000 ALTER TABLE `hawin_servico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_usuario`
--

DROP TABLE IF EXISTS `hawin_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_usuario` (
  `id_usuario` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario` varchar(225) NOT NULL,
  `nome_usuario` varchar(20) NOT NULL,
  `senha` text,
  `id_permissao` int(10) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_usuario`),
  KEY `id_permissao` (`id_permissao`),
  CONSTRAINT `fk_usuario_permissao` FOREIGN KEY (`id_permissao`) REFERENCES `hawin_permissao` (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_usuario`
--

LOCK TABLES `hawin_usuario` WRITE;
/*!40000 ALTER TABLE `hawin_usuario` DISABLE KEYS */;
INSERT INTO `hawin_usuario` VALUES (1,'2017-05-23 23:43:04','08307213479','FELIPE EDUARDO SOUZA','a1a541cf70176fc798dc084208a472bd',1,1),(2,'2017-05-31 23:38:56','08448077458','DERICK PAULINO IRINE','e10adc3949ba59abbe56e057f20f883e',1,1),(3,'2017-06-16 01:38:31','06764891482','FELIPE DOS SANTOS','e10adc3949ba59abbe56e057f20f883e',2,1),(4,'2017-07-10 00:45:16','10322491452','ATHOS BONNER','9f068be548b827138978b897f747e2f4',1,1),(5,'2017-07-10 00:46:30','08419788406','WAGNER FRANÇA','68366609fa3190c4cf5157ac97af9a54',1,1),(6,'2017-07-10 01:54:48','Toni Stark','Stark','e10adc3949ba59abbe56e057f20f883e',5,1),(7,'2017-07-10 23:26:30','Teste aewrwrsg','tsegfsrdht','e10adc3949ba59abbe56e057f20f883e',4,1),(8,'2017-07-14 16:53:42','11111111111','Operador','e10adc3949ba59abbe56e057f20f883e',4,1),(9,'2017-07-14 16:55:21','22222222222','Supervisor','e10adc3949ba59abbe56e057f20f883e',3,1),(10,'2017-07-14 16:56:55','33333333333','Operador APA','e10adc3949ba59abbe56e057f20f883e',5,1),(11,'2017-07-14 16:57:55','44444444444','Diretor','e10adc3949ba59abbe56e057f20f883e',2,1);
/*!40000 ALTER TABLE `hawin_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hawin_vistovenda`
--

DROP TABLE IF EXISTS `hawin_vistovenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hawin_vistovenda` (
  `id_vistovenda` int(10) NOT NULL AUTO_INCREMENT,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome_visto` varchar(225) NOT NULL,
  `descricao` varchar(225) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_vistovenda`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hawin_vistovenda`
--

LOCK TABLES `hawin_vistovenda` WRITE;
/*!40000 ALTER TABLE `hawin_vistovenda` DISABLE KEYS */;
INSERT INTO `hawin_vistovenda` VALUES (1,'2017-06-19 02:02:07','PENDENTE','PENDENTE',1),(2,'2017-06-19 02:02:22','MIGRADO','MIGRADO',1),(3,'2017-07-02 19:19:29','ERRO','ERRO',1);
/*!40000 ALTER TABLE `hawin_vistovenda` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-15 11:34:24
